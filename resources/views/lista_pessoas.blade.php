.@extends('layouts.base')

@section('navbar')
  <div class="menu">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="#">opc1</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">opc2</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('/') }}">Voltar</a>
          </li>
        </ul>
        <div class="my-2 my-lg-0">
          Usuário:@extends('layouts.base')
        </div>
      </div>
    </nav>
  </div>
@endsection

@section('conteudo')
  <div class="w-auto container_login">
    <table class="table">
      <tr>
        <td>Nome</td>
        <td>Matrícula</td>
        <td>Tipo</td>
      </tr>

      @foreach ($pessoas as $pessoa)
        <tr>
          <td> {{ $pessoa->nome}}</td>
          <td> {{ $pessoa->matricula}}</td>
          <td> {{ $pessoa->tipo}} </td>
        </tr>

      @endforeach
    </table>
  </div>
@endsection
